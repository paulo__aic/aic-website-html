<?php
    # Include the Autoloader

    require 'vendor/autoload.php';
    use Mailgun\Mailgun;

    # Instantiate the client.

    $mgClient = new Mailgun("key-338426c91194c1b8f78b11a4062dd943");
    $domain = "api.activeinsights.pt";

    # Geathering all info from clinet-side

    $name = $_POST['form_call_name'];
    $phone = $_POST['form_call_contact'];
    $service = $_POST['form_call_service'];
    $text = $_POST['form_call_textarea'];
    // $checkbox = $_POST['form_call_checkbox'];

    #Generating html for an e-mail

    $emailString = '<h1>Pedido de telefonema</h1>';
	$emailString .= '<h2><strong>Dados:</strong></h2>';
	$emailString .= '<strong>Nome: </strong>'.$name.'<br />';
	$emailString .= '<br />';
	$emailString .= '<strong>Telefone: </strong>'.$phone.'<br />';
	$emailString .= '<br />';
	$emailString .= '<strong>Serviço pretendido: </strong>'.$service.'<br />';
    $emailString .= '<br />';
    $emailString .= '<strong>Informações adicionais: </strong>'.$text.'<br />';
    $emailString .= '<br />';

    // echo $emailString;

    # Make the call to the client.

    $result = $mgClient->sendMessage($domain,
		array(  
			'from'    => 'AI Consulting <mailer@api.activeinsights.pt>',
			'to'      => 'AI Consulting <hello@aiconsulting.pt>',
			'subject' => 'Pedido de telefonema',
			'html'    => $emailString
		)
	);
?>