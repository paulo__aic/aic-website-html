<?php
    # Include the Autoloader

    require 'vendor/autoload.php';
    use Mailgun\Mailgun;

    # Instantiate the client.

    $mgClient = new Mailgun("key-338426c91194c1b8f78b11a4062dd943");
    $domain = "api.activeinsights.pt";

    # Geathering all info from clinet-side

    // print_r($_FILES);

    $name = $_POST['form_candidates_name'];
    $email = $_POST['form_candidates_email'];
    $area = $_POST['form_candidates_area'];
    $fileName = $_FILES['form_candidates_file']['name'];
    $fileUpload = $_FILES['form_candidates_file']['tmp_name'];
    // $checkbox = $_REQUEST['form_checkbox'];

    // print_r($fileUpload);
    
    #Generating html for an e-mail

    $emailString = '<h1>Candidatura</h1>';
    $emailString .= '<h2><strong>Dados:</strong></h2>';
    $emailString .= '<strong>Nome: </strong>'.$name.'<br />';
    $emailString .= '<br />';
    $emailString .= '<strong>E-mail: </strong>'.$email.'<br />';
    $emailString .= '<br />';
    $emailString .= '<strong>Área de formação: </strong>'.$area.'<br />';
    $emailString .= '<br />';
    $emailString .= '<strong>Ficheiro anexado: </strong>'.$fileName.'<br />';
    $emailString .= '<br />';

    // echo $emailString;

    # Make the call to the client.

    $result = $mgClient->sendMessage($domain,
        array(  
            'from'    => 'AI Consulting <mailer@api.activeinsights.pt>',
            'to'      => 'AI Consulting <hello@aiconsulting.pt>',
            'subject' => 'Candidatura',
            'html'    => $emailString
        ),
        array(
            'attachment' => [
                'filePath' => $fileUpload
                // 'remoteName' => $_FILES["form_candidates_file"]["name"]
            ]
        )
    );

?>