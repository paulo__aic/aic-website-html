<?php
    # Include the Autoloader

    require 'vendor/autoload.php';
    use Mailgun\Mailgun;

    # Instantiate the client.

    $mgClient = new Mailgun("key-338426c91194c1b8f78b11a4062dd943");
    $domain = "api.activeinsights.pt";

    # Geathering all info from clinet-side

    $service = $_POST['form_rate_service'];
    $experience = $_POST['form_rate_experience'];
    $contact = $_POST['form_rate_contact'];
    $text = $_POST['form_rate_textarea'];
    // $checkbox = $_POST['form_checkbox'];

    #Generating html for an e-mail

    $emailString = '<h1>Pedido de Rate</h1>';
    $emailString .= '<h2><strong>Dados:</strong></h2>';
    $emailString .= '<strong>Serviço pretendido: </strong>'.$service.'<br />';
    $emailString .= '<br />';
    $emailString .= '<strong>Experiência pretendida: </strong>'.$experience.'<br />';
    $emailString .= '<br />';
    $emailString .= '<strong>Contacto (telefone ou skype): </strong>'.$contact.'<br />';
    $emailString .= '<br />';
    $emailString .= '<strong>Informações adicionais: </strong>'.$text.'<br />';
    $emailString .= '<br />';

    // echo $emailString;

    # Make the call to the client.

    $result = $mgClient->sendMessage($domain,
		array(  
			'from'    => 'AI Consulting <mailer@api.activeinsights.pt>',
			'to'      => 'AI Consulting <hello@aiconsulting.pt>',
			'subject' => 'Pedido de Rate',
			'html'    => $emailString
		)
	);
?>