$(document).ready(function () {

    // Get IE or Edge browser version
    var version = detectIE();

    if (version === false) {
        $('.logo_ie').css('display', 'none');
    } else {
        $('.nav-container svg').css('display', 'none');
        $('.logo_ie').css('display', 'block');
    }

    /**
     * detect IE
     * returns version of IE or false, if browser is not Internet Explorer
     */
    function detectIE() {
        var ua = window.navigator.userAgent;

        // Test values; Uncomment to check result …

        // IE 10
        // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';
        
        // IE 11
        // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';

        var msie = ua.indexOf('MSIE ');
        if (msie > 0) {
            // IE 10 or older => return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }

        var trident = ua.indexOf('Trident/');
        if (trident > 0) {
            // IE 11 => return version number
            var rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }

        // other browser
        return false;
    }


    // modal open from external link
    function openModalLink() {
        if (document.URL.indexOf("https://aiconsulting.pt/#callme") >= 0) {
            console.log('abriu');
            $('.call-contact').addClass('visible');
            $('.full-container').addClass('blur');
            $('.full-container_index').addClass('blur');
            $('.nav-container').css('z-index', '0');
        }
    }

    openModalLink();


    // nav bar behaviour
    function bindMouseEnter() {
        var window_top = 0;
        window_top = $(window).scrollTop();
        element_pos = $('.section-2').offset().top;

        $('.nav-container').off('mouseenter').on('mouseenter', function(event) {
            if (element_pos > window_top) {
                if (event.relatedTarget !== null || $('.burger-button').hasClass('active') === false) {
                    $('.inner-nav-container, nav, #a_graphic, #left-leg, #right-leg, #i_graphic, #i-leg, #active, #insights').addClass('active');
                }
            }
            bindMouseLeave();
        });

        $('.nav-container').on('click', function() {
            if ($('.burger-button').hasClass('active') === false) {
                $('.inner-nav-container, nav, #a_graphic, #left-leg, #right-leg, #i_graphic, #i-leg, #active, #insights').addClass('active');
            }
        });
    }

    bindMouseEnter();

    function bindMouseLeave() {
        var window_top = 0;
        window_top = $(window).scrollTop();
        element_pos = $('.section-2').offset().top;
        
        $('.nav-container').off('mouseleave').on('mouseleave', function(event) { 
            if (element_pos > window_top) {
                if (event.relatedTarget !== null || $('.burger-button').hasClass('active') === false) {
                    $('.inner-nav-container, nav, #a_graphic, #left-leg, #right-leg, #i_graphic, #i-leg, #active, #insights').removeClass('active');
                }
            }
            bindMouseEnter();
        });
    }

    bindMouseLeave()


    // modal control
    //trigger the animation - open modal window
    function bindTriggerOpen() {
        $('[data-type="modal-trigger"]').off('click').one('click', function () {
            $('#a_graphic, #i_graphic, #left-leg, #right-leg, #i-leg, #active, #insights').removeClass('active');

            var actionBtn = $(this),
                scaleValue = retrieveScale(actionBtn.next('.cd-modal-bg'));
            
            actionBtn.next('.cd-modal-bg').addClass('is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function() {
                animateLayer(actionBtn.next('.cd-modal-bg'), scaleValue, true);
            });

            $('.burger-button, .menu-bar').addClass('active');
            $('nav.active').addClass('fixed');
            $('body').addClass('overflow-hidden');

            setTimeout(function() {
                bindTriggerClose();
            }, 1000);
        });
    }

    bindTriggerOpen();


    //trigger the animation - close modal window
    function bindTriggerClose() {
        $('.cd-section .cd-modal-close, a[href*="#"]').off('click').one('click', function() {
            closeModal();

            $('.burger-button, .menu-bar').removeClass('active');

            setTimeout(function() {
                $('#a_graphic, #i_graphic, #left-leg, #right-leg, #i-leg, #active, #insights').addClass('active');
            }, 400);

            $('nav.active').removeClass('fixed');
            $('body').removeClass('overflow-hidden');

            setTimeout(function() {
                bindTriggerOpen();
            }, 1500);
        });
    }

    bindTriggerClose();


    /* update cover layer dimention and position on resize */
    $(window).on('resize', function() {
        if ($('.cd-section.modal-is-visible').length > 0) {
            window.requestAnimationFrame(updateLayer);
        }
    });

    /* retrieve scale */
    function retrieveScale(btn) {
        var btnRadius = btn.width()/2,
            left = btn.offset().left + btnRadius,
            top = btn.offset().top + btnRadius - $(window).scrollTop(),
            scale = scaleValue(top, left, btnRadius, $(window).height(), $(window).width());

        btn.css('position', 'fixed').velocity({
            top: top - btnRadius,
            left: left - btnRadius,
            translateX: 0
        }, 0);

        return scale;
    }

    function scaleValue( topValue, leftValue, radiusValue, windowW, windowH) {
        var maxDistHor = ( leftValue > windowW/2) ? leftValue : (windowW - leftValue),
            maxDistVert = ( topValue > windowH/2) ? topValue : (windowH - topValue);
        return Math.ceil(Math.sqrt( Math.pow(maxDistHor, 2) + Math.pow(maxDistVert, 2) )/radiusValue);
    }

    function animateLayer(layer, scaleVal, bool) {
        layer.velocity({ scale: scaleVal }, 400, function() {
            // $('body').toggleClass('overflow-hidden', bool);
            (bool) 
                ? layer.parents('.cd-section').addClass('modal-is-visible').end().off('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend')
                : layer.removeClass('is-visible').removeAttr( 'style' );
        });
    }

    function updateLayer() {
        var layer = $('.cd-section.modal-is-visible').find('.cd-modal-bg'),
            layerRadius = layer.width()/2,
            layerTop = layer.siblings('.btn').offset().top + layerRadius - $(window).scrollTop(),
            layerLeft = layer.siblings('.btn').offset().left + layerRadius,
            scale = scaleValue(layerTop, layerLeft, layerRadius, $(window).height(), $(window).width());
        
        layer.velocity({
            top: layerTop - layerRadius,
            left: layerLeft - layerRadius,
            scale: scale
        }, 0);
    }

    function closeModal() {
        var section = $('.cd-section.modal-is-visible');
        section.removeClass('modal-is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function() {
            animateLayer(section.find('.cd-modal-bg'), 1, false);
        });
        //if browser doesn't support transitions...
        // if(section.parents('.no-csstransitions').length > 0 ) animateLayer(section.find('.cd-modal-bg'), 1, false);
    }


    // rotating text
    var duration = 3000;
    var current = 1;
    var textWrapper = $(".text-rotation");
    var textItems = textWrapper.children().length;
    var firstText = textWrapper.children().first();
    var rotationInterval = setInterval(rotatingText, duration);

    function rotatingText() {
        var textWrapperHeight = textWrapper.height();
        var interv = current * -1 * textWrapperHeight;
        
        firstText.css("margin-top", interv + "px");
        if (current == textItems) {
            firstText.css("margin-top", "0px");
            current = 1;
        } else {
            current++;
        }
    }


    /* scroll to */
    $('.scroller').on('click', function() {
        $('html, body').animate({ 
            scrollTop: $('.scroller').offset().top + 67
        }, 800);
    });


    // var scroll_pos = 0;
    $(window).scroll(function() {
        checkSecPosition();
    });


    /* checking section position */
    function checkSecPosition() {
        var window_top = 0;
        window_top = $(window).scrollTop();
        element_pos = $('.section-2').offset().top;

        if (element_pos <= window_top) {
            $('.inner-nav-container, nav, #a_graphic, #left-leg, #right-leg, #i_graphic, #i-leg, #active, #insights').addClass('active');
        } else {
            $('.inner-nav-container, nav, #a_graphic, #left-leg, #right-leg, #i_graphic, #i-leg, #active, #insights').removeClass('active');
        }
    }


    /* input control */
    $('.cv-upload').each(function () {
        var $input	 = $(this),
            $label	 = $input.next('label'),
            labelVal = $label.html();

        $input.on('change', function(e) {
            var fileName = '';

            if (this.files && this.files.length > 1) {
                fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
            } else if (e.target.value) {
                fileName = e.target.value.split('\\').pop();
            }

            if (fileName) {
                $label.find('span').html(fileName).css({ 'color': 'black', 'font-weight': '700', 'width': '150%' });
            } else {
                $label.html(labelVal);
            }
        });

        // Firefox bug fix
        $input
        .on('focus', function() {$input.addClass( 'has-focus');})
        .on('blur', function() {$input.removeClass('has-focus');});
    });


    /* form validation */
    $.validator.addMethod("specialChar1", function(value, element) {
        return this.optional(element) || /^[a-záàâãéèêíïóôõöúçñA-ZÁÀÂÃÉÈÊÍÏÓÔÕÖÚÇÑ\s]*$/.test(value);
    }, "Apenas letras");

    $.validator.addMethod("specialChar2", function(value, element) {
        return this.optional(element) || /^[0-9a-záàâãéèêíïóôõöúçñA-ZÁÀÂÃÉÈÊÍÏÓÔÕÖÚÇÑ!?.,;:"'ªº _\s]*$/.test(value);
    }, "Apenas letras, números e pontuação");

    $.validator.addMethod("specialChar3", function(value, element) {
        return this.optional(element) || /^[0-9a-zA-Z.\s]*$/.test(value);
    }, "Apenas letras, números e pontuação");

    $.validator.addMethod("specialChar4", function(value, element) {
        return this.optional(element) || /^[0-9()\-\s]*$/.test(value);
    }, "Insira um telefone válido");

    $.validator.addMethod("minNumbs", function(value, element) {
        "use strict";

        if (this.optional(element)) {
            return true;
        }

        // Removing no number
        value = value.replace(/[^\d]+/g, "");

        // Checking value to have 12 characters only
        if (value.length !== 12) {
            return false;
        }

        if (value === "000000000000" ||
            value === "111111111111" ||
            value === "222222222222" ||
            value === "333333333333" ||
            value === "444444444444" ||
            value === "555555555555" ||
            value === "666666666666" ||
            value === "777777777777" ||
            value === "888888888888" ||
            value === "999999999999" ){
            return false;
        }

        return true;

    }, "Introduza um telefone válido");


    /* section-3 form */
    $('#sec3_form').validate({
        rules:{
            form_name: {
                required: true,
                specialChar1: true,
                maxlength: 30,
                minlength: 2
            },
            form_email: {
                required: true,
                email: true
            },
            form_service: {
                required: true
            },
            form_checkbox: {
                required: true
            }
        },
        submitHandler: function(form) {
            $('.full-container').addClass('blur');
            $('.nav-container').css('z-index', '0');
            $('.form_loader').show();
            var formID = '#' + $('.cta-form-submit').parent()[0].id;
            var formData = $(formID).serialize();
            $.ajax({
                url: 'php/cta-form-sendform.php',
                type: 'POST',
                data: formData,
                cache: false,
                success: function() {
                    var success_message = "<div class='message_wrapper'><i class='far fa-check-circle'></i><p class='success-message'>Formulário submetido com sucesso.</p><p class='success-message'>Obrigado pelo contacto.</p></div>";

                    $('.form_message_container').html(success_message).show();

                    setTimeout(function() {
                        $('.form_loader').hide();
                        $('.full-container').removeClass('blur');
                        $('.nav-container').css('z-index', '9900');
                        $('.form_message_container').fadeOut();
                    }, 5000)
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    // console.log('ERRORS: ' + textStatus);
                    var error_message = "<div class='message_wrapper'><i class='far fa-times-circle'></i><p class='error-message'>Ocorreram erros com o envio.</p><p class='error-message'>Por favor, tente novamente.</p></div>";

                    $('.form_message_container').html(error_message).show();

                    setTimeout(function() {
                        $('.form_loader').hide();
                        $('.full-container').removeClass('blur');
                        $('.nav-container').css('z-index', '9900');
                        $('.form_message_container').fadeOut();
                    }, 5000)

                },
                complete: function() {
                    $('#sec3_form').trigger('reset');
                }
            });
        }
    })


    /* section-7_5 form */
    $('#sec3_5form').validate({
        rules:{
            form_name1: {
                required: true,
                specialChar1: true,
                maxlength: 30,
                minlength: 2
            },
            form_email1: {
                required: true,
                email: true
            },
            form_service1: {
                required: true
            },
            form_checkbox1: {
                required: true
            }
        },
        submitHandler: function(form) {
            $('.full-container').addClass('blur');
            $('.nav-container').css('z-index', '0');
            $('.form_loader').show();
            var formID = '#' + $('.cta-form-submit1').parent()[0].id;
            var formData = $(formID).serialize();
            $.ajax({
                url: 'php/cta-form-sendform1.php',
                type: 'POST',
                data: formData,
                cache: false,
                success: function() {
                    var success_message = "<div class='message_wrapper'><i class='far fa-check-circle'></i><p class='success-message'>Formulário submetido com sucesso.</p><p class='success-message'>Obrigado pelo contacto.</p></div>";

                    $('.form_message_container').html(success_message).show();

                    setTimeout(function() {
                        $('.form_loader').hide();
                        $('.full-container').removeClass('blur');
                        $('.nav-container').css('z-index', '9900');
                        $('.form_message_container').fadeOut();
                    }, 5000)
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    // console.log('ERRORS: ' + textStatus);
                    var error_message = "<div class='message_wrapper'><i class='far fa-times-circle'></i><p class='error-message'>Ocorreram erros com o envio.</p><p class='error-message'>Por favor, tente novamente.</p></div>";

                    $('.form_message_container').html(error_message).show();

                    setTimeout(function() {
                        $('.form_loader').hide();
                        $('.full-container').removeClass('blur');
                        $('.nav-container').css('z-index', '9900');
                        $('.form_message_container').fadeOut();
                    }, 5000)

                },
                complete: function() {
                    $('#sec3_5form').trigger('reset');
                }
            });
        }
    })


    /* call */
    /* phone formatter.js */
    $('#form_call_contact').formatter({
        'pattern': '({{999}}) {{999}}-{{999}}-{{999}}',
        'persistent': false
    });

    $('#form_call').validate({
        rules:{
            form_call_name: {
                required: true,
                specialChar1: true,
                maxlength: 30,
                minlength: 2
            },
            form_call_contact: {
                required: true,
                specialChar4: true,
                minNumbs: true
            },
            form_call_service: {
                required: true
            },
            form_call_textarea: {
                specialChar2: true,
                maxlength: 150    
            },
            form_call_checkbox: {
                required: true
            }
        },
        submitHandler: function() {
            $('.full-container').addClass('blur');
            $('.nav-container').css('z-index', '0');
            $('.form_loader').show();
            var formID = '#' + $('.form_call_submit').parent()[0].id;
            var formData = $(formID).serialize();
            $.ajax({
                url: 'php/call-form-sendform.php',
                type: 'POST',
                data: formData,
                cache: false,
                success: function () {
                    var success_message = "<div class='message_wrapper'><i class='far fa-check-circle'></i><p class='success-message'>Formulário submetido com sucesso.</p><p class='success-message'>Obrigado pelo contacto.</p></div>";

                    $('.form_message_container').html(success_message).show();

                    setTimeout(function() {
                        $('.form_loader').hide();
                        $('.call-contact').removeClass('visible');
                        $('.full-container').removeClass('blur');
                        $('.nav-container').css('z-index', '9900');
                        $('.form_message_container').fadeOut();
                    }, 5000)
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    // console.log('ERRORS: ' + textStatus);
                    var error_message = "<div class='message_wrapper'><i class='far fa-times-circle'></i><p class='error-message'>Ocorreram erros com o envio.</p><p class='error-message'>Por favor, tente novamente.</p></div>";

                    $('.form_message_container').html(error_message).show();

                    setTimeout(function() {
                        $('.form_loader').hide();
                        $('.full-container').removeClass('blur');
                        $('.nav-container').css('z-index', '9900');
                        $('.form_message_container').fadeOut();
                    }, 5000)

                },
                complete: function() {
                    $('#form_call').trigger('reset');
                }
            });
        }
    })


    /* rate */
    $('#form_rate').validate({
        rules:{
            form_rate_service: {
                required: true
            },
            form_rate_experience: {
                required: true
            },
            form_rate_contact: {
                required: true,
                specialChar3: true,
                maxlength: 30
            },
            form_rate_textarea: {
                specialChar2: true,
                maxlength: 150
            },
            form_rate_checkbox: {
                required: true
            }
        },
        submitHandler: function(form) {
            $('.full-container').addClass('blur');
            $('.nav-container').css('z-index', '0');
            $('.form_loader').show();
            var formID = '#' + $('.form_rate_submit').parent()[0].id;
            var formData = $(formID).serialize();
            $.ajax({
                url: 'php/rate-form-sendform.php',
                type: 'POST',
                data: formData,
                cache: false,
                success: function() {
                    var success_message = "<div class='message_wrapper'><i class='far fa-check-circle'></i><p class='success-message'>Formulário submetido com sucesso.</p><p class='success-message'>Obrigado pelo contacto.</p></div>";

                    $('.form_message_container').html(success_message).show();

                    setTimeout(function() {
                        $('.form_loader').hide();
                        $('.rate-contact').removeClass('visible');
                        $('.full-container').removeClass('blur');
                        $('.nav-container').css('z-index', '9900');
                        $('.form_message_container').fadeOut();
                    }, 5000)
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    // console.log('ERRORS: ' + textStatus);
                    var error_message = "<div class='message_wrapper'><i class='far fa-times-circle'></i><p class='error-message'>Ocorreram erros com o envio.</p><p class='error-message'>Por favor, tente novamente.</p></div>";

                    $('.form_message_container').html(error_message).show();

                    setTimeout(function() {
                        $('.form_loader').hide();
                        $('.full-container').removeClass('blur');
                        $('.nav-container').css('z-index', '9900');
                        $('.form_message_container').fadeOut();
                    }, 5000)

                },
                complete: function() {
                    $('#form_rate').trigger('reset');
                }
            });
        }
    })


    /* candidates */
    $('#form_candidates').validate({
        rules:{
            form_candidates_name: {
                required: true,
                specialChar1: true,
                maxlength: 30,
                minlength: 2
            },
            form_candidates_email: {
                required: true,
                email: true
            },
            form_candidates_area: {
                required: true
            },
            form_candidates_file: {
                required: true,
                accept: "application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword, application/pdf"
            },
            form_candidates_checkbox: {
                required: true
            }
        },
        submitHandler: function() {
            $('.full-container').addClass('blur');
            $('.nav-container').css('z-index', '0');
            $('.form_loader').show();
            
            var form = document.getElementById("form_candidates");

            var formData = new FormData(form);

            /* code for object indexing check */
            // for (var [key, value] of formData.entries()) { 
            //     console.log(key, value);
            // }

            $.ajax({
                url: 'php/candidates-form-sendform.php',
                method: "post",
                data: formData,
                contentType: false,
                processData: false,
                cache: false,
                success: function() {
                    var success_message = "<div class='message_wrapper'><i class='far fa-check-circle'></i><p class='success-message'>Formulário submetido com sucesso.</p><p class='success-message'>Obrigado pelo contacto.</p></div>";

                    $('.form_message_container').html(success_message).show();

                    setTimeout(function() {
                        $('.form_loader').hide();
                        $('.full-container').removeClass('blur');
                        $('.nav-container').css('z-index', '9900');
                        $('.form_message_container').fadeOut();
                    }, 5000)
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    // console.log('ERRORS: ' + textStatus);
                    var error_message = "<div class='message_wrapper'><i class='far fa-times-circle'></i><p class='error-message'>Ocorreram erros com o envio.</p><p class='error-message'>Por favor, tente novamente.</p></div>";

                    $('.form_message_container').html(error_message).show();

                    setTimeout(function() {
                        $('.form_loader').hide();
                        $('.full-container').removeClass('blur');
                        $('.nav-container').css('z-index', '9900');
                        $('.form_message_container').fadeOut();
                    }, 5000)

                },
                complete: function() {
                    $('#form_candidates').trigger('reset');
                    $('.cv-upload').next('label').find('span').html('Anexar CV (PDF ou WORD)').css({
                        'font-weight': '300',
                        'color': '#afb4b9'
                    });
                }
            });
        }
    })


    /* inner modal control */
    /* call modal */
    $('.call-button, .call-button_mobile').on('click', function() {
        $('.call-contact').addClass('visible');
        $('.full-container').addClass('blur');
        $('.full-container_index').addClass('blur');
        $('.nav-container').css('z-index', '0');
    });

    $('.modalclose').on('click', function() {
        $('.call-contact').removeClass('visible');
        $('.full-container').removeClass('blur');
        $('.full-container_index').removeClass('blur');
        $('.nav-container').css('z-index', '9900');
    });

    /* rate modal */
    $('.rate, .rate-trigger').on('click', function() {
        $('.rate-contact').addClass('visible');
        $('.full-container').addClass('blur');
        $('.nav-container').css('z-index', '0');
    });

    $('.modalclose').on('click', function() {
        $('.rate-contact').removeClass('visible');
        $('.full-container').removeClass('blur');
        $('.nav-container').css('z-index', '9900');
    });
    

    /* slick control */
    $(".regular").slick({
        dots: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 601,
                settings: {
                    arrows: false
                }
            }
        ]
    });


    /* activating pac-man */
    // $(".sec8-img").on('click', function () {
    //     console.log('pac-man');
    // });


    /* scroll to ID */
    // $('a[href*="#"]').on('click', function (e) {
    // 	e.preventDefault();

    // 	$('html, body').animate({
    // 		scrollTop: $($(this).attr('href')).offset().top
    // 	}, 500, 'swing');
    // });

// end of ready function
});